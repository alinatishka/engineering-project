<?php
	session_start();
	require_once "php/includes/connect.php";
	if(!isset($_GET['type']) and !isset($_GET['id'])){
		header("Location: index.php");
	}
?>
<!DOCTYPE html>
<html lang="ru">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="../libs/css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/style.css">
	<title>ConceptNews</title>
</head>

<body>
	<header class="header">
	<div class="header__login header_in <?php
															if(isset($_SESSION['user'])){
																echo "header__login_disabled";
															}
														?>">
			<a href="#" data-toggle="modal" data-target="#form-sign-in">ВОЙТИ</a>
			<a href="#" data-toggle="modal" data-target="#form-sign-up">ЗАРЕГИСТРИРОВАТЬСЯ</a>
		</div>
		<div class="header__login header_in <?php
															if(!isset($_SESSION['user'])){
																echo "header__login_disabled";
															}
														?>">
			<a href="/profile.php"><img src="/img/ip/ico/account.svg" alt=""></a>
			<a href="php/includes/logout.php" class="pb-0">ВЫЙТИ</a>
		</div>
		<h2 class="header__title">ConceptNews</h2>
		<div class="burger">
			<span></span>
		</div>
		<nav class="menu">
					<?php
						include_once "php/urls/menu.php"
					?>
		</nav>
	</header>
	<?php
			$type = $_GET["type"];
			$id = $_GET["id"];
			switch($type){
				case "film":
					$type = "кино";
					break;
				case "sport":
					$type = "спорт";
					break;
				case "music":
					$type = "музыка";
					break;
				case "history":
					$type = "история";
					break;
				case "travel":
					$type = "путешествия";
					break;
				case "art":
					$type = "искусство";
					break;
				case "mode":
					$type = "мода";
					break;
				case "business":
					$type = "бизнес";
					break;
				case "tech":
					$type = "технологии";
					break;
				case "politic":
					$type = "политика";
					break;
			}
			
			$data = mysqli_fetch_all(mysqli_query($connect, "SELECT * FROM articles JOIN authors ON articles.author_id = authors.id JOIN themes ON articles.theme_id = themes.id WHERE themes.name = '$type' AND articles.id = '$id'"))[0];
			$datas = mysqli_fetch_all(mysqli_query($connect, "SELECT * FROM articles JOIN authors ON articles.author_id = authors.id JOIN themes ON articles.theme_id = themes.id"));
		?>
	<main class="container-fluid my-container">
		<section class="content_acc">
			<div class="content__title">
				<h2 class=""><?=$data[3]?></h2>
				<?php
					switch($_SESSION["user"]["type"]){
						case "admin":
							include_once "php/urls/edit.php";
							break;
						case "author":
							include_once "php/urls/edit.php";
							break;
					}
				?>
			</div>
			<div class="rewiews all">
				<div class="wrap__items">
					<div class="rewiews__item">
						<div class="wrap-img wrap-img_color">
							<img src="../php/files/<?=$data[6]?>" alt="">
							<p class="mt-3 pl-2"><?=$data[5]?></p>
							<p class="pl-2 pb-3">Автор: <?=mb_strtoupper($data[8])?></p>
						</div>
						<div class="rewiews__wrap-text">
							<p class="rewiews__text"><?=$data[4]?></p>
						</div>
					</div>
					<div class="rewiews__item anothe_articles_wrap">
						<div class="news-right anothe_articles">
						<div class="news-right">
						<?php echo '
							<div class="news-box2">
								<a class="news-box2__link" href="/article.php/?type=music&id='.$datas[1][0].'">
									<span class="news-box2__date">'.$datas[1][5].'</span>
									<div class="news-box2__inner">
										<div class="news-box2__wrap-img">
											<img src="/php/files/'.$datas[1][6].'">
										</div>
										<div class="wrap-text">
											<h3 class="news-box2__title">'.$datas[1][3].'</h3>
											<p class="news-box2__text">'.str_replace("�", "", mb_substr($datas[1][4], 0, 50)).'</p>
										</div>
									</div>
								</a>
							</div>
							<div class="news-box2">
								<a class="news-box2__link" href="/article.php/?type=politic&id='.$datas[2][0].'">
									<span class="news-box2__date">'.$datas[2][5].'</span>
									<div class="news-box2__inner">
										<div class="news-box2__wrap-img">
											<img src="/php/files/'.$datas[2][6].'">
										</div>
										<div class="wrap-text">
											<h3 class="news-box2__title">'.$datas[2][3].'</h3>
											<p class="news-box2__text">'.str_replace("�", "", mb_substr($datas[2][4], 0, 50)).'</p>
										</div>
									</div>
								</a>
							</div>
							<div class="news-box2">
								<a class="news-box2__link" href="/article.php/?type=politic&id='.$datas[3][0].'">
									<span class="news-box2__date">'.$datas[3][5].'</span>
									<div class="news-box2__inner">
										<div class="news-box2__wrap-img">
											<img src="/php/files/'.$datas[3][6].'">
										</div>
										<div class="wrap-text">
											<h3 class="news-box2__title">'.$datas[3][3].'</h3>
											<p class="news-box2__text">'.str_replace("�", "", mb_substr($datas[3][4], 0, 50)).' </p>
										</div>
									</div>
								</a>
							</div>
						</div>
						';?>
						</div>
					</div>
				</div>
			</div>
		</section>
	</main>
	<footer class="footer">
		<div class="container mt-0 footer-wrap">
			<h2 class="footer__title">ConceptNews</h2>
			<p class="footer__text">
				<span>Тишкина Алина Федоровна, 191-322</span>
				<span>&#169; 2020 Все права защищены</span>
			</p>
		</div>
	</footer>

	<div class="modal fade" id="edit" tabindex="-1" role="dialog"
		aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Редактирование</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="/php/includes/form_edit.php/?type=<?=$_GET["type"]?>&id=<?=$_GET["id"]?>" method="POST" enctype="multipart/form-data">
						<div class="form-group">
							<label>Заголовок: </label>
							<input type='text' class='form-control' placeholder='Введите данные' name='edit1'>
							<label>Описание: </label>
							<input type='text' class='form-control' placeholder='Введите данные' name='edit2'>
						</div>
						<label for="">Изображение: </label>
						<div class = "custom-file">
							<input type="file" class="custom-file-input" id="customFile" name = "edit3">
							<label class="custom-file-label" for="customFile">Choose file</label>
						</div>
						<div class="form-check p-0 mb-3">
							<label class="form-check-label">Подтверждаю свои действия
								<input class="form-check-input ml-2" type="checkbox" name = "editable">
							</label>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
							<input type="submit" value="Сохранить" class="btn btn-primary">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- ФОРМА ВХОДА -->
	<div class="modal fade  my-modal" id="form-sign-in">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h2 class="modal-title">ВХОД</h2>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action = "php/auth/signin.php" method = "post">
						<label for="enter-log" class="col-form-label">E-mail:</label>
						<input type="text" class="form-control" id="enter-log" placeholder="Введите ваш e-mail" name = "email">
						<label for="enter-pass" class="col-form-label">Пароль:</label>
						<input type="password" class="form-control" id="enter-pass" placeholder="Введите ваш пароль" name = "password">
            <div class="modal-footer">
              <p class="d-none">Введите правильный пароль!</p>
              <button type="submit" class="btn my-modal__btn px-4 py-1">Войти</button>
              <button type="button" class="d-none btn my-modal__btn px-4 py-1">Назад</button>
            </div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- ФОРМА РЕГИСТРАЦИИ -->
	<div class="modal fade my-modal" id="form-sign-up">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">РЕГИСТРАЦИЯ</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action = "php/register/signup.php" method = "post">
						<div class="form-group">
							<label for="reg-name" class="col-form-label"><span class="text-danger">*</span>Введите ваше имя и
								фамилию:</label>
							<input type="text" class="form-control" id="reg-name" placeholder="Введите ваше ФИО"  name = "name">
						</div>
						<div class="form-group">
							<label for="reg-mail" class="col-form-label"><span class="text-danger">*</span>E-mail:</label>
							<input type="email" class="form-control" id="reg-mail" placeholder="Введите ваш e-mail"  name = "email">
						</div>
						<div class="form-group">
							<label for="reg-pass" class="col-form-label"><span class="text-danger">*</span>Пароль:</label>
							<input type="password" class="form-control" id="reg-pass" placeholder="Придумайте надежный пароль" name = "pass1">
						</div>
						<div class="form-group">
							<label for="reg-pass-confirm" class="col-form-label"><span class="text-danger">*</span>Повторите ваш
								пароль:</label>
							<input type="password" class="form-control" id="reg-pass-confirm" placeholder="Повторите пароль" name = "pass2">
						</div>
						<div class="form-group"><span class="text-danger">*</span><span>Зарегистрироваться как:</span><br>
							<span style="font-size: .8rem;">(если вы хотите предложить новость, то выберите РЕДАКТОР)</span>
							<select class="custom-select" name="type" id="select-theme" required>
								<option selected disabled>Выберите роль</option>
								<option value="user">Пользователь</option>
								<option value="author">Редактор</option>
							</select>
						</div>
						<div class="modal-footer justify-content-center mt-3">
							<button type="submit" class="btn my-modal__btn">Зарегистрироваться</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<script src="../libs/js/jquery-3.5.1.min.js"></script>
	<script src="../libs/js/bootstrap.min.js"></script>
	<script src="../js/main.js"></script>
</body>

</html>