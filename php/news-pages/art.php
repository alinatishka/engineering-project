<?php
  $type_ar = "";
  switch($_GET["type"]){
    case "film":
      $type_ar = "кино";
      break;
    case "sport":
      $type_ar = "спорт";
      break;
    case "music":
      $type_ar = "музыка";
      break;
    case "history":
      $type_ar = "история";
      break;
    case "travel":
      $type_ar = "путешествия";
      break;
    case "art":
      $type_ar = "искусство";
      break;
    case "mode":
      $type_ar = "мода";
      break;
    case "business":
      $type_ar = "бизнес";
      break;
    case "tech":
      $type_ar = "технологии";
      break;
    case "politic":
      $type_ar = "политика";
      break;
  }
  if(isset($_GET["search"])){
    $search = base64_decode($_GET["search"]);
    $data = mysqli_query($connect, "SELECT * FROM articles JOIN authors ON articles.author_id = authors.id JOIN themes ON articles.theme_id = themes.id WHERE themes.name = '$type_ar' AND articles.title LIKE "."'".$search."%'"."");
  }else{
    $data = mysqli_query($connect, "SELECT * FROM articles JOIN authors ON articles.author_id = authors.id JOIN themes ON articles.theme_id = themes.id WHERE themes.name = '$type_ar'");
  }
  $rows = mysqli_fetch_all($data);
  if(isset($_SESSION["user"])){
    if($_SESSION["user"]["type"] == "user"){
      $id = $rows[0][1];
      echo '<div class="content__title">
              <h2 class="">'.mb_strtoupper($type_ar).'</h2>
              <a href="php/includes/subscribe.php/?id='.$id.'"><img class="mx-3" src="../img/ip/ico/sub.svg" alt=""></a>
            </div>';
    }
    else{
      echo '<div class="content__title">
              <h2 class="">'.mb_strtoupper($type_ar).'</h2>
            </div>';
    }
  }
  else{
    echo '<div class="content__title">
            <h2 class="">'.mb_strtoupper($type_ar).'</h2>
          </div>';
  }
  $type = $_GET["type"];
  echo '<div class="sidebar all" style="grid-row: 2/3;">
        <div class="sidebar__wrap">
          <div class="sidebar__item">
            <h3 class="sidebar__title">ФИЛЬТРАЦИЯ</h3>
            <span class="sidebar__text">Дата:</span>
            <div class="row justify-content-center mt-4">
              <label class="sidebar__date">ОТ<input class="ml-3 px-2" type="text" placeholder="ГГГГ-ММ-ДД"></label>
              <label class="sidebar__date">ДО<input class="ml-3 px-2" type="text" placeholder="ГГГГ-ММ-ДД"></label>
            </div>
            <button class="sidebar__btn watch py-1 mt-3">ПРИМЕНИТЬ</button>
          </div>
          <form class="search mt-4" action = "/php/includes/filter.php/?type='.$type.'" method = "post">
            <input class="search__input" type="search" placeholder="ПОИСК..." name = "search">
            <button type = "submit" class="search__btn">
              <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M14.6771 12.9102C15.6612 11.5662 16.2499 9.91535 16.2499 8.12572C16.2499 3.64575 12.6049 0.000732422 8.12494 0.000732422C3.64498 0.000732422 0 3.64575 0 8.12572C0 12.6057 3.64502 16.2507 8.12498 16.2507C9.91461 16.2507 11.5656 15.6619 12.9096 14.6778L18.2324 20.0006L20 18.2331C20 18.233 14.6771 12.9102 14.6771 12.9102ZM8.12498 13.7507C5.02318 13.7507 2.50001 11.2275 2.50001 8.12572C2.50001 5.02391 5.02318 2.50074 8.12498 2.50074C11.2268 2.50074 13.75 5.02391 13.75 8.12572C13.75 11.2275 11.2267 13.7507 8.12498 13.7507Z" />
              </svg>
            </button>
          </form>
        </div>
      </div>
      <div class="rewiews all">
      <div class="wrap__items">';
  for($i=0; $i< mysqli_num_rows($data); $i++){
    echo '<div class="rewiews__item">
    <span class="news-box2__date">'.$rows[$i][5].'</span>
    <div class="wrap-img">
      <a class="wrap-img__link" href="article.php/?type='.$_GET["type"].'&id='.$rows[$i][0].'">
        <span></span>
      </a>
      <img src="php/files/'.$rows[$i][6].'" alt="">
    </div>
    <div class="rewiews__wrap-text">
      <h3 class="rewiews__subtitle">'.$rows[$i][3].'</h3>
      <p class="rewiews__text">'.str_replace("�", "", mb_substr($rows[$i][4], 0, 150)).'</p>
    </div>
  </div>';
  }
  echo '</div>
        </div>';
?>
