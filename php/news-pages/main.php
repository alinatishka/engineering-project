<?php
$data = mysqli_fetch_all(mysqli_query($connect, "SELECT * FROM articles JOIN authors ON articles.author_id = authors.id JOIN themes ON articles.theme_id = themes.id"));
	echo '<section class="content">
	<div class="news all">
		<h3 class="news__title">Новости</h3>
		<div class="news-box1" style="background: url(php/files/'.$data[0][6].') center;">
			<a href="/article.php/?type=travel&id='.$data[0][0].'">
				<div class="news-box1__content">
					<span>'.$data[0][5].'</span>
					<h3 class="news-box1__header">'.$data[0][3].'</h3>
					<p class="news-box1__text">
					'.str_replace("�", "", mb_substr($data[0][4], 0, 50)).'
					</p>
				</div>
			</a>
		</div>
		<div class="news-right">
			<div class="news-box2">
				<a class="news-box2__link" href="/article.php/?type=music&id='.$data[1][0].'">
					<span class="news-box2__date">'.$data[1][5].'</span>
					<div class="news-box2__inner">
						<div class="news-box2__wrap-img">
							<img src="php/files/'.$data[1][6].'">
						</div>
						<div class="wrap-text">
							<h3 class="news-box2__title">'.$data[1][3].'</h3>
							<p class="news-box2__text">'.str_replace("�", "", mb_substr($data[1][4], 0, 50)).'</p>
						</div>
					</div>
				</a>
			</div>
			<div class="news-box2">
				<a class="news-box2__link" href="/article.php/?type=politic&id='.$data[2][0].'">
					<span class="news-box2__date">'.$data[2][5].'</span>
					<div class="news-box2__inner">
						<div class="news-box2__wrap-img">
							<img src="php/files/'.$data[2][6].'">
						</div>
						<div class="wrap-text">
							<h3 class="news-box2__title">'.$data[2][3].'</h3>
							<p class="news-box2__text">'.str_replace("�", "", mb_substr($data[2][4], 0, 50)).'</p>
						</div>
					</div>
				</a>
			</div>
			<div class="news-box2">
				<a class="news-box2__link" href="/article.php/?type=politic&id='.$data[3][0].'">
					<span class="news-box2__date">'.$data[3][5].'</span>
					<div class="news-box2__inner">
						<div class="news-box2__wrap-img">
							<img src="php/files/'.$data[3][6].'">
						</div>
						<div class="wrap-text">
							<h3 class="news-box2__title">'.$data[3][3].'</h3>
							<p class="news-box2__text">'.str_replace("�", "", mb_substr($data[3][4], 0, 50)).' </p>
						</div>
					</div>
				</a>
			</div>
		</div>
	</div>
	<div class="sidebar all">
		<div class="sidebar__wrap">
			<div class="sidebar__item">
				<h3 class="sidebar__title">ФИЛЬТРАЦИЯ</h3>
				<span class="sidebar__text">Дата:</span>
				<div class="row justify-content-center mt-4">
					<label class="sidebar__date">ОТ<input class="ml-3 px-2" type="text" placeholder="ГГГГ-ММ-ДД"></label>
					<label class="sidebar__date">ДО<input class="ml-3 px-2" type="text" placeholder="ГГГГ-ММ-ДД"></label>
				</div>
				<button class="sidebar__btn watch py-1 mt-3">ПРИМЕНИТЬ</button>
			</div>
			<div class="search mt-4">
				<input class="search__input" type="search" placeholder="ПОИСК...">
				<button class="search__btn">
					<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path
							d="M14.6771 12.9102C15.6612 11.5662 16.2499 9.91535 16.2499 8.12572C16.2499 3.64575 12.6049 0.000732422 8.12494 0.000732422C3.64498 0.000732422 0 3.64575 0 8.12572C0 12.6057 3.64502 16.2507 8.12498 16.2507C9.91461 16.2507 11.5656 15.6619 12.9096 14.6778L18.2324 20.0006L20 18.2331C20 18.233 14.6771 12.9102 14.6771 12.9102ZM8.12498 13.7507C5.02318 13.7507 2.50001 11.2275 2.50001 8.12572C2.50001 5.02391 5.02318 2.50074 8.12498 2.50074C11.2268 2.50074 13.75 5.02391 13.75 8.12572C13.75 11.2275 11.2267 13.7507 8.12498 13.7507Z" />
					</svg>
				</button>
			</div>
		</div>
	</div>
	<div class="rewiews all">
		<div class="wrap__items">
			<div class="rewiews__item">
				<span class="news-box2__date">'.$data[4][5].'</span>
				<div class="wrap-img">
					<a class="wrap-img__link" href="/article.php/?type=sport&id='.$data[4][0].'">
						<span></span>
					</a>
					<img src="php/files/'.$data[4][6].'" alt="">
				</div>
				<div class="rewiews__wrap-text">
					<h3 class="rewiews__subtitle">'.$data[4][3].'</h3>
					<p class="rewiews__text">'.str_replace("�", "", mb_substr($data[4][4], 0, 50)).'</p>
				</div>
			</div>
			<div class="rewiews__item">
				<span class="news-box2__date">'.$data[5][5].'</span>
				<div class="wrap-img">
					<a class="wrap-img__link" href="/article.php/?type=travel&id='.$data[5][0].'">
						<span></span>
					</a>
					<img src="php/files/'.$data[5][6].'" alt="">
				</div>
				<div class="rewiews__wrap-text">
					<h3 class="rewiews__subtitle">'.$data[5][3].'</h3>
					<p class="rewiews__text">'.str_replace("�", "", mb_substr($data[5][4], 0, 50)).'</p>
				</div>
			</div>
			<div class="rewiews__item">
				<span class="news-box2__date">'.$data[6][5].'</span>
				<div class="wrap-img">
					<a class="wrap-img__link" href="/article.php/?type=film&id='.$data[6][0].'">
						<span></span>
					</a>
					<img src="php/files/'.$data[6][6].'" alt="">
				</div>
				<div class="rewiews__wrap-text">
					<h3 class="rewiews__subtitle">'.$data[6][3].'</h3>
					<p class="rewiews__text">'.str_replace("�", "", mb_substr($data[6][4], 0, 50)).'</p>
				</div>
			</div>
			<div class="rewiews__item">
				<span class="news-box2__date">'.$data[7][5].'</span>
				<div class="wrap-img">
					<a class="wrap-img__link" href="/article.php/?type=business&id='.$data[7][0].'">
						<span></span>
					</a>
					<img src="php/files/'.$data[7][6].'" alt="">
				</div>
				<div class="rewiews__wrap-text">
					<h3 class="rewiews__subtitle">'.$data[7][3].'</h3>
					<p class="rewiews__text">'.str_replace("�", "", mb_substr($data[7][4], 0, 50)).'</p>
				</div>
			</div>
		</div>
	</div>
</section>';
?>