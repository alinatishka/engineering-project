<?php
      echo '<form class="col-md-8 text-body font-weight-bold row" action = "php/includes/add.php" method = "post" enctype="multipart/form-data">
      <div class="form-group">
        <label class="mb-0 mt-3" for="select-theme">ВЫБЕРИТЕ ТЕМУ:</label>
        <div class="w-100"></div>
        <select class="custom-select col-md-8" id="select-theme" required  name = "type">
        <option selected disabled>ВЫБЕРИТЕ ТЕМУ:</option>
            <option value="1">СПОРТ</option>
            <option value="2">МУЗЫКА</option>
            <option value="3">КИНО</option>
            <option value="4">ПУТЕШЕСТВИЯ</option>
            <option value="5">БИЗНЕС</option>
            <option value="6">ИСТОРИЯ</option>
            <option value="7">ИСКУССТВО</option>
            <option value="8">МОДА</option>
            <option value="9">ПОЛИТИКА</option>
            <option value="10">ТЕХНОЛОГИИ</option>
        </select>
        <label class="w-100 mb-0 mt-3" for="name-article">ЗАГОЛОВОК НОВОСТИ:</label>
        <div class="w-100"></div>
        <input type="text" class="form-control col-md-8" id="name-article" placeholder="ВВЕДИТЕ НАЗВАНИЕ СТАТЬИ"
          value="" required name = "title">
        <label class="mb-0 mt-3" for="description">ТЕКСТ НОВОСТИ:</label>
        <textarea class="form-control description-articles" id="description"
          placeholder="ВВЕДИТЕ ОПИСАНИЕ СТАТЬИ" name = "description"></textarea>
        <label class="mb-0 mt-3" for="download-file">ФОТО К НОВОСТИ:</label>
        <div class="custom-file">
          <input type="file" class="custom-file-input col-md-8" id="download-file" required name = "img">
          <label class="custom-file-label col-md-8" for="download-file">Прикрепите фото к посту</label>
        </div>
        <div class="modal-footer p-0 mt-4" style="border: solid 0;">
          <button type="submit" class="btn my-modal__btn ml-auto mt-3">ВЫЛОЖИТЬ</button>
        </div>
      </div>
    </form>';
?>