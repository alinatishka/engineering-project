<div class="row mr-auto ml-4 mb-2 header__login header_out <?php
															if(isset($_SESSION['user'])){
																echo "header__login_disabled";
															}
														?>">
				<a href="#" data-toggle="modal" data-target="#form-sign-in">ВОЙТИ</a>
				<div class="w-100"></div>
				<a href="#" data-toggle="modal" data-target="#form-sign-up">ЗАРЕГИСТРИРОВАТЬСЯ</a>
			</div>
			<div class="row mr-auto ml-4 mb-2 header__login header_out <?php
															if(!isset($_SESSION['user'])){
																echo "header__login_disabled";
															}
														?>">
				<a href="/profile.php"><img src="img/ip/ico/account.svg" alt=""></a>
				<a href="php/includes/logout.php" class="pb-0">ВЫЙТИ</a>
			</div>
			<a href="/" class="menu__item">ГЛАВНАЯ<img src="img/header/arrow-right.jpg" alt=""></a>
			<a href="/?type=music" class="menu__item">МУЗЫКА<img src="img/header/arrow-right.jpg" alt=""></a>
			<a href="/?type=film" class="menu__item">КИНО<img src="img/header/arrow-right.jpg" alt=""></a>
			<a href="/?type=history" class="menu__item">ИСТОРИЯ<img src="img/header/arrow-right.jpg" alt=""></a>
			<a href="/?type=art" class="menu__item">ИСКУССТВО<img src="img/header/arrow-right.jpg" alt=""></a>
			<a href="/?type=sport" class="menu__item">СПОРТ<img src="img/header/arrow-right.jpg" alt=""></a>
			<a href="/?type=politic" class="menu__item">ПОЛИТИКА <img src="img/header/arrow-right.jpg" alt=""></a>
			<a href="/?type=travel" class="menu__item">ПУТЕШЕСТВИЯ <img src="img/header/arrow-right.jpg" alt=""></a>
			<a href="/?type=mode" class="menu__item">МОДА<img src="img/header/arrow-right.jpg" alt=""></a>
			<a href="/?type=business" class="menu__item">БИЗНЕС<img src="img/header/arrow-right.jpg" alt=""></a>
			<a href="/?type=tech" class="menu__item">ТЕХНОЛОГИИ<img src="img/header/arrow-right.jpg" alt=""></a>
			<div class="sidebar__item header_out">
				<h3 class="sidebar__title">ФИЛЬТРАЦИЯ</h3>
				<span class="sidebar__text">Дата:</span>
				<div class="sidebar__label-wrap mt-4">
					<label class="sidebar__date">ОТ<input class="ml-3 px-2" type="text" placeholder="ГГГГ-ММ-ДД"></label>
					<label class="sidebar__date">ДО<input class="ml-3 px-2" type="text" placeholder="ГГГГ-ММ-ДД"></label>
				</div>
				<button class="sidebar__btn watch py-1 mt-3">ПРИМЕНИТЬ</button>
			</div>
			<form class="search mt-4 header_out" action = "/php/includes/filter.php/?type=<?=$type?>" method = "post">
				<input class="search__input" type="search" placeholder="ПОИСК...">
				<button type = "submit" class="search__btn"><img src="/img/ip/ico/search.svg" alt=""></button>
			</form>