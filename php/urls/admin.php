<?php
  if(!isset($_GET["who"])){
    $col_a = mysqli_fetch_all(mysqli_query($connect, "SELECT * FROM `authors`"));
    $col_n = mysqli_fetch_all(mysqli_query($connect, "SELECT * FROM `articles`"));
    $col_u = mysqli_fetch_all(mysqli_query($connect, "SELECT * FROM `users`"));

    echo '<div class="account__text">
            <span>Выберите любую таблицу, чтобы посмотреть/удалить/отредактировать информацию.</span>
          </div>
          <div class="account__table mt-4">
            <a href="?who=users">ПОЛЬЗОВАТЕЛИ</a>
            <a href="?who=articles">НОВОСТИ</a>
            <a href="?who=authors">РЕДАКТОРЫ</a>
          </div>
          <div class="like">
							<p class="mt-5">Всего пользователей: '.count($col_u).'</p>
							<table class="table table-bordered col-md-4">
								<thead>
									<tr>
										<th scope="col">Всего новостей:</th>
										<th scope="col">'.count($col_n).'</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th scope="col">Всего редакторов:</th>
										<th scope="col">'.count($col_a).'</th>
									</tr>
								</tbody>
							</table>
						</div>';
  }
  else{
    switch($_GET["who"]){
      case "authors":
        $arr = mysqli_fetch_all(mysqli_query($connect, "SELECT * FROM `authors`")); 
        $names = mysqli_fetch_assoc(mysqli_query($connect, "SELECT * FROM `authors`")); 
        echo "<div style = 'overflow-x: scroll;'><table class = 'table'>
        <thead class = 'thead-dark'>
          <tr>";
            for($i = 0; $i < count(array_keys($names)); $i++){
              echo "<th scope='col'>".array_keys($names)[$i]."</th>";
            }
          echo "</tr>
        </thead>
        <tbody>";
          foreach($arr as $value){
            echo "<tr>";
            for($i = 0; $i < count(array_keys($value)); $i++){
              echo "<td>".$value[array_keys($value)[$i]]."</td>";
            }
            echo "</tr>";
          }
        echo "</tbody>
      </table></div>"; 
        break;
      case "users":
        $arr = mysqli_fetch_all(mysqli_query($connect, "SELECT * FROM `users`")); 
        $names = mysqli_fetch_assoc(mysqli_query($connect, "SELECT * FROM `users`")); 
        echo "<div style = 'overflow-x: scroll;'><table class = 'table'>
        <thead class = 'thead-dark'>
          <tr>";
            for($i = 0; $i < count(array_keys($names)); $i++){
              echo "<th scope='col'>".array_keys($names)[$i]."</th>";
            }
          echo "</tr>
        </thead>
        <tbody>";
          foreach($arr as $value){
            echo "<tr>";
            for($i = 0; $i < count(array_keys($value)); $i++){
              echo "<td>".$value[array_keys($value)[$i]]."</td>";
            }
            echo "</tr>";
          }
        echo "</tbody>
      </table></div>"; 
        break;
      case "articles":
        $arr = mysqli_fetch_all(mysqli_query($connect, "SELECT * FROM `articles`")); 
        $names = mysqli_fetch_assoc(mysqli_query($connect, "SELECT * FROM `articles`")); 
        echo "<div style = 'overflow-x: scroll;'><table class = 'table'>
        <thead class = 'thead-dark'>
          <tr>";
            for($i = 0; $i < count(array_keys($names)); $i++){
              echo "<th scope='col'>".array_keys($names)[$i]."</th>";
            }
          echo "</tr>
        </thead>
        <tbody>";
          foreach($arr as $value){
            echo "<tr>";
            for($i = 0; $i < count(array_keys($value)); $i++){
              echo "<td>".$value[array_keys($value)[$i]]."</td>";
            }
            echo "</tr>";
          }
        echo "</tbody>
      </table></div>"; 
        break;
    }
  }
?>