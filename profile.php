<?php 
	session_start();
	require_once "php/includes/connect.php";
	if(!isset($_SESSION["user"])){
		header("Location: index.php");
	} 
?>
<!DOCTYPE html>
<html lang="ru">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="../libs/css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/style.css">
	<title>ConceptNews</title>
</head>

<body>
			  <!-- Модальное окно -->
				<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
					aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLongTitle">Удаление</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form action = "/php/includes/delete.php/?who=<?=$_GET["who"]?>" method = "POST">
								<div class="form-group">
										<label for="disabledSelect">Выберете</label>
										<select id="disabledSelect" class="form-control" name = "option">
											<option>Выбрать</option>
											<?php
												if($_GET["who"] == "authors"){
													$arr = mysqli_fetch_assoc(mysqli_query($connect, "SELECT * FROM `authors`")); 
												}
												else if($_GET["who"] == "users"){
													$arr = mysqli_fetch_assoc(mysqli_query($connect, "SELECT * FROM `users`")); 
												}
												else if($_GET["who"] == "articles"){
													$arr = mysqli_fetch_assoc(mysqli_query($connect, "SELECT * FROM `articles`")); 
												}
												for($i = 0; $i < count(array_keys($arr)); $i++){
													echo "<option value = '".array_keys($arr)[$i]."'>".array_keys($arr)[$i]."</option>";
												}
											?>
										</select>
									</div><br>
									<div class="form-group">
										<label for="disabledTextInput">Данные</label>
										<input type="text" id="disabledTextInput" class="form-control" placeholder="Disabled input" name = "text">
									</div>
									<div class="form-check">
										<label class="form-check-label">Подтверждаю свои действия
											<input class="form-check-input ml-2" type="checkbox" name = "enabled">
										</label>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
										<input type = "submit" value = "Удалить" class="btn btn-danger">
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				    <!-- Модальное окно изменения -->
				<div class="modal fade" id="edit" tabindex="-1" role="dialog"
					aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLongTitle">Редактирование</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form action="/php/includes/edit.php/?who=articles" method="POST">
									<div class="form-group">
										<label>Введите ID
										</label>
										<input type="text" class="form-control" placeholder="Введите ID" name="unique_id">
									</div>
									<div class="form-group">
									<?php
										$arr = mysqli_fetch_assoc(mysqli_query($connect, "SELECT * FROM `articles`")); 
											for($i = 0; $i < count(array_keys($arr)); $i++){
												if($_GET["who"] == "articles" && $i == 0){
													continue;
												}
												echo "<label>".array_keys($arr)[$i]."</label>";
												echo "<input type='text' class='form-control' placeholder='Введите данные' name='edit".$i."'>";
											}
										?>
									</div>
									<div class="form-check p-0 mb-3">
										<label class="form-check-label">Подтверждаю свои действия
											<input class="form-check-input ml-2" type="checkbox" name = "editable">
										</label>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
										<input type="submit" value="Сохранить" class="btn btn-primary">
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
	<header class="header">
		<div class="header__login header_in">
			<a href="/profile.php"><img src="../img/ip/ico/account.svg" alt=""></a>
			<a href="/php/includes/logout.php" class="pb-0">ВЫЙТИ</a>
		</div>
		<h2 class="header__title">ConceptNews</h2>
		<div class="burger">
			<span></span>
		</div>
		<nav class="menu">
		<?php
						include_once "php/urls/menu.php"
					?>
		</nav>
	</header>
	<main class="container-fluid my-container mt-3">
		<section class="content">
			<div class="content__title">
				<h4 class="">ЛИЧНЫЙ КАБИНЕТ</h4>
				<?php
					switch($_GET["who"]){
						case "authors":
							echo "<button data-toggle='modal' data-target='#exampleModalCenter' style='background: none; border: none;'>
							<img style='max-width: 30px; width: 100%;' src='../img/ip/ico/delete.svg' title='Удаление записи' alt='Удаление записи'>
							</button>";
							break;
						case "users":
							echo "<button data-toggle='modal' data-target='#exampleModalCenter' style='background: none; border: none;'>
							<img style='max-width: 30px; width: 100%;' src='../img/ip/ico/delete.svg' title='Удаление записи' alt='Удаление записи'>
							</button>";
							break;
						case "articles":
							echo "<button data-toggle='modal' data-target='#exampleModalCenter' style='background: none; border: none;'>
							<img style='max-width: 30px; width: 100%;' src='/img/ip/ico/delete.svg' title='Удаление записи' alt='Удаление записи'>
							</button>
							<button style='background: none; border: none;' data-toggle='modal' data-target='#edit' ><img style='max-width: 30px; width: 100%;' src='../img/ip/ico/update.svg' title='Редактирование записи' alt='Редактирование записи'></button>";
							break;
					}
				?>
			</div>
			<div class="rewiews all">
				<div class="wrap__items my-font-one">
					<div class="rewiews__item">
						<?php
							require_once "php/includes/connect.php";
							switch($_SESSION["user"]["type"]){
								case "admin":
									include_once "php/urls/admin.php";
									break;
								case "author":
									include_once "php/urls/authors.php";
									break;
								case "user":
									include_once "php/urls/subs.php";
									break;
							}
						?>
					</div>
				</div>
			</div>
		</section>
	</main>
	<footer class="footer">
		<div class="container mt-0 footer-wrap">
			<h2 class="footer__title">ConceptNews</h2>
			<p class="footer__text">
				<span>Тишкина Алина Федоровна, 191-322</span>
				<span>&#169; 2020 Все права защищены</span>
			</p>
		</div>
	</footer>

	<script src="../libs/js/jquery-3.5.1.min.js"></script>
	<script src="../libs/js/bootstrap.min.js"></script>
	<script src="../js/main.js"></script>
</body>

</html>