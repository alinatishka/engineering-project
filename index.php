<?php

	session_start();
//    require_once('./db/db.php');
	require_once "php/includes/connect.php";

	$type = $_GET["type"];
	if(!isset($_GET["type"])){
		$type = "";
	}

?>
<!DOCTYPE html>
<html lang="ru">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="libs/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">
	<title>ConceptNews</title>
</head>

<body>
	<header class="header">
		<div class="header__login header_in <?php
															if(isset($_SESSION['user'])){
																echo "header__login_disabled";
															}
														?>">
			<a href="#" data-toggle="modal" data-target="#form-sign-in">ВОЙТИ</a>
			<a href="#" data-toggle="modal" data-target="#form-sign-up">ЗАРЕГИСТРИРОВАТЬСЯ</a>
		</div>
		<div class="header__login header_in <?php
															if(!isset($_SESSION['user'])){
																echo "header__login_disabled";
															}
														?>">
			<a href="/profile.php"><img src="img/ip/ico/account.svg" alt=""></a>
			<a href="php/includes/logout.php" class="pb-0">ВЫЙТИ</a>
		</div>
		<h2 class="header__title">ConceptNews</h2>
		<div class="burger">
			<span></span>
		</div>
		<nav class="menu">
          <?php
						include_once "php/urls/menu.php"
					?>
		</nav>
			<?php
				if(!isset($_GET["type"])){
					$data = mysqli_fetch_all(mysqli_query($connect, "SELECT * FROM articles JOIN authors ON articles.author_id = authors.id JOIN themes ON articles.theme_id = themes.id"));
					echo '<div id="head-slider" class="carousel slide header-slider" data-ride="carousel">
					<ol class="carousel-indicators">
						<li class="carousel-indicators__item active" data-target="#head-slider" data-slide-to="0"></li>
						<li class="carousel-indicators__item mx-4" data-target="#head-slider" data-slide-to="1"></li>
						<li class="carousel-indicators__item" data-target="#head-slider" data-slide-to="2"></li>
					</ol>
					<div class="carousel-inner">
						<a href="#" class="carousel-item active">
							<img class="d-block" src="php/files/'.$data[0][6].'" alt="Первый слайд">
							<div class="carousel-caption d-none d-md-block">
								<h3>'.$data[0][3].'</h3>
							</div>
						</a>
						<a href="#" class="carousel-item">
							<img class="d-block" src="php/files/'.$data[1][6].'" alt="Первый слайд">
							<div class="carousel-caption d-none d-md-block">
								<h3>'.$data[1][3].'</h3>
							</div>
						</a>
						<a href="#" class="carousel-item">
							<img class="d-block" src="php/files/'.$data[2][6].'" alt="Первый слайд">
							<div class="carousel-caption d-none d-md-block">
								<h3>'.$data[2][3].'</h3>
							</div>
						</a>
					</div>
					<div class="header-slider__wrap-links">
						<a class="carousel-control-prev" href="#head-slider" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Предыдущий</span>
						</a>
						<a class="carousel-control-next" href="#head-slider" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Следующий</span>
						</a>
					</div>
				</div>';
				}
			?>
	</header>
	<main class="container-fluid my-container">
      <?php
				if(isset($_GET["type"])){
          echo '<section class="content">';
          include_once "php/news-pages/art.php";
          echo '</section>';
				}
				else{
					include_once "php/news-pages/main.php";
				}
			?>
	</main>
	<footer class="footer">
		<div class="container mt-0 footer-wrap">
			<h2 class="footer__title">ConceptNews</h2>
			<p class="footer__text">
				<span>Тишкина Алина Федоровна, 191-322</span>
				<span>&#169; 2020 Все права защищены</span>
			</p>
		</div>
	</footer>

	<!-- ФОРМА ВХОДА -->
	<div class="modal fade  my-modal" id="form-sign-in">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h2 class="modal-title">ВХОД</h2>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action = "php/auth/signin.php" method = "post">
						<label for="enter-log" class="col-form-label">E-mail:</label>
						<input type="text" class="form-control" id="enter-log" placeholder="Введите ваш e-mail" name = "email">
						<label for="enter-pass" class="col-form-label">Пароль:</label>
						<input type="password" class="form-control" id="enter-pass" placeholder="Введите ваш пароль" name = "password">
            <div class="modal-footer">
              <p class="d-none">Введите правильный пароль!</p>
              <button type="submit" class="btn my-modal__btn px-4 py-1">Войти</button>
              <button type="button" class="d-none btn my-modal__btn px-4 py-1">Назад</button>
            </div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- ФОРМА РЕГИСТРАЦИИ -->
	<div class="modal fade my-modal" id="form-sign-up">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">РЕГИСТРАЦИЯ</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action = "php/register/signup.php" method = "post">
						<div class="form-group">
							<label for="reg-name" class="col-form-label"><span class="text-danger">*</span>Введите ваше имя и
								фамилию:</label>
							<input type="text" class="form-control" id="reg-name" placeholder="Введите ваше ФИО"  name = "name">
						</div>
						<div class="form-group">
							<label for="reg-mail" class="col-form-label"><span class="text-danger">*</span>E-mail:</label>
							<input type="email" class="form-control" id="reg-mail" placeholder="Введите ваш e-mail"  name = "email">
						</div>
						<div class="form-group">
							<label for="reg-pass" class="col-form-label"><span class="text-danger">*</span>Пароль:</label>
							<input type="password" class="form-control" id="reg-pass" placeholder="Придумайте надежный пароль" name = "pass1">
						</div>
						<div class="form-group">
							<label for="reg-pass-confirm" class="col-form-label"><span class="text-danger">*</span>Повторите ваш
								пароль:</label>
							<input type="password" class="form-control" id="reg-pass-confirm" placeholder="Повторите пароль" name = "pass2">
						</div>
						<div class="form-group"><span class="text-danger">*</span><span>Зарегистрироваться как:</span><br>
							<span style="font-size: .8rem;">(если вы хотите предложить новость, то выберите РЕДАКТОР)</span>
							<select class="custom-select" name="type" id="select-theme" required>
								<option selected disabled>Выберите роль</option>
								<option value="user">Пользователь</option>
								<option value="author">Редактор</option>
							</select>
						</div>
						<div class="modal-footer justify-content-center mt-3">
							<button type="submit" class="btn my-modal__btn">Зарегистрироваться</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<script src="libs/js/jquery-3.5.1.min.js"></script>
	<script src="libs/js/bootstrap.min.js"></script>
	<script src="js/main.js"></script>
</body>

</html>